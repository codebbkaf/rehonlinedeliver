//
//  ViewController.swift
//  IDF-proj
//
//  Created by HankTseng on 2018/3/25.
//  Copyright © 2018年 HankTseng. All rights reserved.
//

import UIKit
import JavaScriptCore
import Toast_Swift
import Firebase

class ViewController: UIViewController, UIWebViewDelegate, JavaScriptFuncProtocol, CallNativeDelegate, NoNetWorkingDelegate {
    
    @IBOutlet weak var grayImageView: UIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    var resetLock = Timer()
    var showDescription = false
    var isTrickingAppler = true
    var ref: DatabaseReference!
    @IBInspectable
    private var alphaI: CGFloat = 0.0
    
    

    
    @IBOutlet weak var webView: UIWebView! {
        didSet {
            webView.delegate = self
        }
    }
    
    var networkManager = NetworkManager.shared
    var qrCodeFrameView: UIView?
    var QRCodeScaneString = ""
    var scanStart = 0
    let loginUrl = URL(string: "https://hnh4u.weebly.com/?fbclid=IwAR184EL2MPlsGtQg6x6dGNoy4SFY8qIaYKG56v3_6dX3hmNPjRwtETmXp4k#")
    let recordUrl = URL(string: "https://hnh4u.weebly.com/?fbclid=IwAR184EL2MPlsGtQg6x6dGNoy4SFY8qIaYKG56v3_6dX3hmNPjRwtETmXp4k#")
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        ref.child("showDescription").setValue(["isShow":true])
        ref.child("isShowYo").observeSingleEvent(of: .value) { (snapshot) in
            print(snapshot.value)
            if let isShow = snapshot.value as? Bool {
                self.isTrickingAppler = isShow
            }
            
        }
        
        networkManager.delegate = self
        
        let isLogin = UserDefaults.standard.object(forKey: "isLogin") as? String ?? "false"
        var request = NSURLRequest(url: loginUrl!)
        if isLogin == "true" {
            request = NSURLRequest(url: recordUrl!)
        }
        
        scanStart = 0
        self.webView?.loadRequest(request as URLRequest)
        signInAnonymously()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func showDescriptionLabel() {
        if isTrickingAppler {
            if !showDescription {
                self.showDescription = true
                resetLock = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.fadeIn), userInfo: nil, repeats: true)
                let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
                impliesAnimation.values = [1.0 ,1.02, 0.98, 1.01, 0.99, 1.005, 1.0]
                impliesAnimation.duration = 1.0
                impliesAnimation.calculationMode = kCAAnimationCubic
            }
        }
    }
    
    @objc func fadeIn(){
        
        if alphaI < 2.5 {
            alphaI += 0.1
        }else{
            alphaI = 0.0
            resetLock.invalidate()
            
        }
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !networkManager.isReachability() {
            self.showToast()
        }
    }
    
    func signInAnonymously() {
        Auth.auth().signInAnonymously { (user, error) in
            if error != nil {
                print("error happen in \(String(describing: user)), \(String(describing: error))")
            } else {
                let uid = user?.uid
                print("login with uid: \(String(describing: uid))")
            }
        }
    }
    
    @IBAction func ButtonForCameraClick(_ sender: UIButton) {
        callCamera()
    }
    
    @IBAction func openCameraButtonClick(_ sender: UIButton) {
        if scanStart == 0 {
            scanStart = 1
        } else {
            scanStart = 0
        }
    }
    
    func callCamera() {
        if isTrickingAppler {
            if scanStart == 0 {
                scanStart = 1
            } else {
                scanStart = 0
            }
        }
    }
    
    func calJSTOFillForm() {
        // 獲得當前頁面中的上下文
        let jsContext = self.webView.value(forKeyPath: "documentView.webView.mainFrame.javaScriptContext") as? JSContext
        
        let fillForm = String(format: "document.getElementsByName('login_name')[0].value = 'yourAccount';document.getElementsByName('login_id')[0].value = 'yourPsd'")
        self.webView.stringByEvaluatingJavaScript(from: fillForm)
        
        jsContext?.objectForKeyedSubscript("fromNative")!.call(withArguments: ["Random: \(Int(arc4random_uniform(10) + 1))"])
    }
    
    func showToast() {
        var style = ToastStyle()
        style.messageColor = .blue
        self.grayImageView.alpha = 0.5
        self.view.makeToast("請確認您的網路連線狀況", duration: 180.0, position: .center)
        ToastManager.shared.isTapToDismissEnabled = false
        ToastManager.shared.isQueueEnabled = true
        
    }
    
    func refreshWebView() {
        self.view.hideToast()
        self.grayImageView.alpha = 0.0
        let isLogin = UserDefaults.standard.object(forKey: "isLogin") as? String ?? "false"
        var request = NSURLRequest(url: loginUrl!)
        if isLogin == "true" {
            request = NSURLRequest(url: recordUrl!)
        }
        
        scanStart = 0
        self.webView?.loadRequest(request as URLRequest)
    }
    
   
    
    
    func deviceVibrate() {
    }
    
    func openCamera(_ parameter1: String) {
        if scanStart == 0 {
            scanStart = 1
        } else {
            scanStart = 0
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let javaScriptMethode = JavaScriptMethode()
        let jsContext = self.webView?.value(forKeyPath: "documentView.webView.mainFrame.javaScriptContext") as? JSContext
        jsContext?.setObject(javaScriptMethode, forKeyedSubscript: "javaScriptCallToSwift" as (NSCopying & NSObjectProtocol)!)
        javaScriptMethode.delegate = self
        //calJSTOFillForm()
        UIView.animate(withDuration: 0.7, animations: {
            
            self.iconImageView.alpha = 0.0
        }) { (completion) in
            UIView.animate(withDuration: 0.2, animations: {
                self.webView.alpha = 1.0
            }, completion: { (completion) in
                self.showDescriptionLabel()
                self.iconImageView.isHidden = true
            })
            
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
}

