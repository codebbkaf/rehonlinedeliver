//
//  JavaScriptMethode.swift
//  IDF-proj
//
//  Created by HankTseng on 2018/3/25.
//  Copyright © 2018年 HankTseng. All rights reserved.
//

import Foundation

class JavaScriptMethode: NSObject, JavaScriptFuncProtocol {
   
    var delegate: CallNativeDelegate? = nil
    
    func openCamera(_ parameter1: String) {
        
        print("parameter1: \(parameter1)")
        delegate?.callCamera()
    }
}
