//
//  Protocols.swift
//  IDF-proj
//
//  Created by HankTseng on 2018/3/25.
//  Copyright © 2018年 HankTseng. All rights reserved.
//

import Foundation
import JavaScriptCore

@objc protocol JavaScriptFuncProtocol: JSExport {
    func openCamera(_ parameter1: String)
}

protocol CallNativeDelegate {
    func callCamera()
}

protocol NoNetWorkingDelegate {
    func showToast()
    func refreshWebView()
}


