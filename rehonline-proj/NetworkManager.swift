//
//  NetworkManager.swift
//  IDF-proj
//
//  Created by HankTseng on 2018/3/26.
//  Copyright © 2018年 HankTseng. All rights reserved.
//

import Foundation
import Reachability

class NetworkManager {
    
    private static var mInstance: NetworkManager?
    static var shared: NetworkManager {
        get {
            if mInstance == nil {
                mInstance = NetworkManager()
            }
            return mInstance!
        }
    }
    var reach: Reachability?
    var delegate: NoNetWorkingDelegate? = nil
    
    func isReachability() -> Bool {
        let reachability = Reachability(hostName: "https://idfactory.tw/app/recent-record.html")
        if reachability?.currentReachabilityStatus().rawValue == 0 {
            return false
        } else {
            return true
        }
    }
    
    func checkNetWork() {
        self.reach = Reachability.forInternetConnection()
        self.reach?.reachableBlock = {
            (reach: Reachability?) -> Void in
            DispatchQueue.main.async {
                self.delegate?.refreshWebView()
                print("reachability")
            }
        }
        self.reach?.unreachableBlock = {
            (reach: Reachability?) -> Void in
            DispatchQueue.main.async {
                self.delegate?.showToast()
                print("unreachability")
            }
        }
        self.reach?.startNotifier()
    }
    
    init() {
        checkNetWork()
    }
}
